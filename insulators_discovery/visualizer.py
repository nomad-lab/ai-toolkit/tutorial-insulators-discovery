import plotly.graph_objects as go
import ipywidgets as widgets
from jupyter_jsmol import JsmolView
import numpy as np
from IPython.display import display, HTML, FileLink
import os
import pandas as pd


class Visualizer:

    def __init__(self, df_selected):

        self.df_selected = df_selected
        self.marker_size = 7
        self.marker_symbol_cls0 = 'circle'
        self.marker_symbol_cls1 = 'square'
        self.symbols = [
            'circle',
            'square',
            'triangle-up',
            'triangle-down',
            'circle-cross',
            'circle-x'
        ]
        self.font_size = 12
        self.cross_size = 15
        self.line_width = 1
        self.font_families = ['Source Sans Pro',
                              'Helvetica',
                              'Open Sans',
                              'Times New Roman',
                              'Arial',
                              'Verdana',
                              'Courier New',
                              'Comic Sans MS',
                              ]
        self.line_styles = ["dash",
                            "solid",
                            "dot",
                            "longdash",
                            "dashdot",
                            "longdashdot"]
        self.gradient_list = ['Blue to red',
                              'Blue to green',
                              'Green to red',
                              'Grey scale',
                              'Purple scale',
                              'Turquoise scale']
        self.bg_color = 'rgba(229,236,246, 0.5)'
        self.color_cls1 = "#EB8273"
        self.color_cls0 = "rgb(138, 147, 248)"

        # self.total_features = sisso.n_dim
        # self.feat_val_list = list(reversed([feat.value for feat in sisso.models[sisso.n_dim - 1][0].feats]))
        # self.features = list(reversed([str(feat) for feat in sisso.models[sisso.n_dim - 1][0].feats]))
        # self.df_selected = pd.DataFrame()
        # for feat, values in zip(self.features, self.feat_val_list):
        #     self.df_selected[feat] = values
        # self.target_predict = sisso.models[sisso.n_dim - 1][0].fit
        # self.target_train = sisso.models[sisso.n_dim - 1][0].prop_train
        #
        # self.coefficients = list(reversed(sisso.models[sisso.n_dim - 1][0].coefs[0][:-1]))
        # self.intercept = sisso.models[sisso.n_dim - 1][0].coefs[0][-1]

        self.total_features = 2
        self.features = ['Feat_0', 'Feat_1']
        self.feat_val_list = df_selected[self.features]

        # self.df_selected['Structure'] = df.reset_index()['min_struc_type']
        # self.df_selected['Chem Formula'] = df.index.to_numpy()

        self.df_cls0 = df_selected['Classification'] == 0
        self.df_cls1 = df_selected['Classification'] == 1

        self.bg_toggle = True
        self.compounds_list = df_selected['Compound'].to_list()
        self.npoints_cls0 = len(self.df_cls0)
        self.npoints_cls1 = len(self.df_cls1)
        self.symbols_cls0 = [self.marker_symbol_cls0] * self.npoints_cls0
        self.symbols_cls1 = [self.marker_symbol_cls1] * self.npoints_cls1
        self.sizes_cls0 = [self.marker_size] * self.npoints_cls0
        self.sizes_cls1 = [self.marker_size] * self.npoints_cls1
        self.colors_cls0 = [self.color_cls0] * self.npoints_cls0
        self.colors_cls1 = [self.color_cls1] * self.npoints_cls1

        self.fig = go.FigureWidget()
        self.viewer_l = JsmolView()
        self.viewer_r = JsmolView()
        self.instantiate_widgets()
        # line_x, line_y = self.f_x(self.features[0], self.features[1])
        # self.line_x = line_x
        # self.line_y = line_y
        print('hi')
        x_hullvx_cls0 = [0.68060E+04,
                    0.68060E+04,
                    0.69360E+04,
                    0.83000E+04,
                    0.83000E+04,
                    0.94640E+04,
                    0.17264E+05,
                    0.17264E+05,
                    0.17264E+05,
                    0.12272E+05,
                    0.94640E+04,
                    0.78000E+04,
                    0.72060E+04]
        y_hullvx_cls0 = [
                    0.18347E+02,
                    0.18347E+02,
                    0.28195E+02,
                    0.66772E+02,
                    0.66772E+02,
                    0.72881E+02,
                    0.70416E+02,
                    0.32634E+02,
                    0.51485E+01,
                    0.35512E+01,
                    0.28624E+01,
                    0.28624E+01,
                    0.73945E+01]

        x_hullvx_cls1 = [            
                0.32640E+04,
                0.37760E+04,
                0.53120E+04,
                0.78000E+04,
                0.78540E+04,
                0.78540E+04,
                0.68060E+04,
                0.68060E+04,
                0.61880E+04,
                0.53120E+04,
                0.48120E+04]
        
        y_hullvx_cls1 = [
            0.82732E+01,
            0.10083E+03,
            0.10083E+03,
            0.72881E+02,
            0.67551E+02,
            0.67551E+02,
            0.27366E+02,
            0.27366E+02,
            0.11541E+02,
            0.88713E+01,
            0.81101E+01,
        ]

        n_intervals = 100

        self.xhull_cls0 = np.array([x_hullvx_cls0[0]])
        self.yhull_cls0 = np.array([y_hullvx_cls0[0]])
        for xy in zip(x_hullvx_cls0, y_hullvx_cls0):
            self.xhull_cls0 = np.concatenate([self.xhull_cls0, np.linspace(self.xhull_cls0[-1], xy[0], n_intervals)])
            self.yhull_cls0 = np.concatenate([self.yhull_cls0, np.linspace(self.yhull_cls0[-1], xy[1], n_intervals)])
        self.xhull_cls0 = np.concatenate([self.xhull_cls0, np.linspace(self.xhull_cls0[-1], x_hullvx_cls0[0], n_intervals)])
        self.yhull_cls0 = np.concatenate([self.yhull_cls0, np.linspace(self.yhull_cls0[-1], y_hullvx_cls0[0], n_intervals)])

        self.xhull_cls1 = np.array([x_hullvx_cls1[0]])
        self.yhull_cls1 = np.array([y_hullvx_cls1[0]])
        for xy in zip(x_hullvx_cls1, y_hullvx_cls1):
            self.xhull_cls1 = np.concatenate([self.xhull_cls1, np.linspace(self.xhull_cls1[-1], xy[0], n_intervals)])
            self.yhull_cls1 = np.concatenate([self.yhull_cls1, np.linspace(self.yhull_cls1[-1], xy[1], n_intervals)])
        self.xhull_cls1 = np.concatenate(
            [self.xhull_cls1, np.linspace(self.xhull_cls1[-1], x_hullvx_cls1[0], n_intervals)])
        self.yhull_cls1 = np.concatenate(
            [self.yhull_cls1, np.linspace(self.yhull_cls1[-1], y_hullvx_cls1[0], n_intervals)])

        # custom_cls0 = np.dstack((self.target_train[self.df_cls0],
        #                          self.target_predict[self.df_cls0]))[0]
        # custom_cls1 = np.dstack((self.target_train[self.df_cls1],
        #                          self.target_predict[self.df_cls1]))[0]

        # the final plot is the sum of two traces, respectively containing the RS vs ZB materials
        x_cls0 = self.feat_val_list[self.features[0]][self.df_cls0]
        y_cls0 = self.feat_val_list[self.features[1]][self.df_cls0]
        x_cls1 = self.feat_val_list[self.features[0]][self.df_cls1]
        y_cls1 = self.feat_val_list[self.features[1]][self.df_cls1]
        self.fig.add_trace(
            (
                go.Scatter(
                    mode='markers',
                    x=x_cls0,
                    y=y_cls0,
                    # customdata=custom_cls0,
                    # text=df[self.df_cls0].index.to_numpy(),
                    # hovertemplate=
                    # r"<b>%{text}</b><br><br>" +
                    # "x axis: %{x:,.2f}<br>" +
                    # "y axis: %{y:,.2f}<br>",
                    # "ΔE reference:  %{customdata[0]:,.4f}<br>" +
                    # "ΔE predicted:  %{customdata[1]:,.4f}<br>"

                    # name='RS',
                    marker=dict(color=self.colors_cls0),
                )
            ))
        self.fig.add_trace(
            (
                go.Scatter(
                    mode='markers',
                    x=x_cls1,
                    y=y_cls1,
                    # customdata=custom_cls1,
                    # text=df[self.df_cls1].index.to_numpy(),
                    # hovertemplate=
                    # r"<b>%{text}</b><br><br>" +
                    # "x axis: %{x:,.2f}<br>" +
                    # "y axis: %{y:,.2f}<br>",
                    # # "ΔE reference:  %{customdata[0]:,.4f}<br>" +
                    # # "ΔE predicted:  %{customdata[1]:,.4f}<br>",
                    # name='ZB',
                    marker=dict(color=self.colors_cls1),
                )
            ))
        self.fig.add_trace(
            go.Scatter(
                x=self.xhull_cls0,
                y=self.yhull_cls0,
                line=dict(color='Grey', width=1, dash=self.line_styles[0]),
                name=r'Convex' + '<br>' + 'hull 0',
                visible=True
            ),
        )
        self.fig.add_trace(
            go.Scatter(
                x=self.xhull_cls1,
                y=self.yhull_cls1,
                line=dict(color='Grey', width=1, dash=self.line_styles[0]),
                name=r'Convex' + '<br>' + 'hull 1',
                visible=True
            ),
        )
        x_min = min(min(x_cls0), min(x_cls1))
        y_min = min(min(y_cls0), min(y_cls1))
        x_max = max(max(x_cls0), max(x_cls1))
        y_max = max(max(y_cls0), max(y_cls1))
        x_delta = 0.05 * abs(x_max - x_min)
        y_delta = 0.05 * abs(y_max - y_min)
        self.fig.update_layout(
            plot_bgcolor=self.bg_color,
            font=dict(
                size=int(self.font_size),
                family=self.font_families[0]
            ),
            xaxis_title=self.features[0],
            yaxis_title=self.features[1],
            xaxis_range=[x_min - x_delta, x_max + x_delta],
            yaxis_range=[y_min - y_delta, y_max + y_delta],
            hoverlabel=dict(
                bgcolor="white",
                font_size=16,
                font_family="Rockwell"
            ),
            width=800,
            height=400,
            margin=dict(
                l=50,
                r=50,
                b=70,
                t=20,
                pad=4
            ),
        )

        self.scatter_cls0 = self.fig.data[0]
        self.scatter_cls1 = self.fig.data[1]
        self.scatter_hull0 = self.fig.data[2]
        self.scatter_hull1 = self.fig.data[3]

        if self.total_features == 2:
            self.scatter_hull0.visible = True
            self.scatter_hull1.visible = True
        else:
            self.widg_linewidth.disabled = True
            self.widg_linestyle.disabled = True

        self.update_markers()

    def f_x(self, feat_x, feat_y):

        idx_x = self.features.index(feat_x)
        idx_y = self.features.index(feat_y)
        line_x = np.linspace(self.df_selected[feat_x].min(), self.df_selected[feat_x].max(), 1000)

        # Gives the classifications line
        if self.widg_featx.value == self.widg_featy.value:
            return line_x, line_x
        else:
            # print('x',  self.coefficients[self.current_features[idx_x]])
            # print('y',  self.coefficients[self.current_features[idx_y]])
            line_y = -line_x * self.coefficients[idx_x] / self.coefficients[idx_y] - self.intercept / self.coefficients[
                idx_y]
            return line_x, line_y

    def update_markers(self):
        # Markers size and symbol are updated simultaneously
        with self.fig.batch_update():
            self.scatter_cls0.marker.size = self.sizes_cls0
            self.scatter_cls1.marker.size = self.sizes_cls1
            self.scatter_cls0.marker.symbol = self.symbols_cls0
            self.scatter_cls1.marker.symbol = self.symbols_cls1
            self.scatter_cls0.marker.color = self.colors_cls0
            self.scatter_cls1.marker.color = self.colors_cls1

    def set_markers_size(self, feature='Default size'):
        # Defines the size of the markers based on the input feature.
        # In case of default feature all markers have the same size.
        # Points marked with x/cross are set with a specific size

        if feature == 'Default size':

            sizes_cls0 = [self.marker_size] * self.npoints_cls0
            sizes_cls1 = [self.marker_size] * self.npoints_cls1
            symbols_cls0 = self.symbols_cls0
            symbols_cls1 = self.symbols_cls1

            try:
                point = symbols_cls0.index('x')
                sizes_cls0[point] = self.cross_size
            except:
                try:
                    point = symbols_cls1.index('x')
                    sizes_cls1[point] = self.cross_size
                except:
                    pass
            try:
                point = symbols_cls0.index('cross')
                sizes_cls0[point] = self.cross_size
            except:
                try:
                    point = symbols_cls1.index('cross')
                    sizes_cls1[point] = self.cross_size
                except:
                    pass

            self.sizes_cls0 = sizes_cls0
            self.sizes_cls1 = sizes_cls1
        else:
            min_value = min(min(self.df_selected.loc[self.df_selected['Structure'] == 'RS'][feature]),
                            min(self.df_selected.loc[self.df_selected['Structure'] == 'ZB'][feature]))
            max_value = max(max(self.df_selected.loc[self.df_selected['Structure'] == 'RS'][feature]),
                            max(self.df_selected.loc[self.df_selected['Structure'] == 'ZB'][feature]))
            coeff = 2 * self.marker_size / (max_value - min_value)
            sizes_cls0 = self.marker_size / 2 + coeff * self.df_selected.loc[self.df_selected['Structure'] == 'RS'][
                feature]
            sizes_cls1 = self.marker_size / 2 + coeff * self.df_selected.loc[self.df_selected['Structure'] == 'ZB'][
                feature]
            self.sizes_cls0 = sizes_cls0
            self.sizes_cls1 = sizes_cls1

    def make_colors(self, feature, gradient):

        if feature == 'Default color':

            self.colors_cls0 = [self.color_cls0] * self.npoints_cls0
            self.colors_cls1 = [self.color_cls1] * self.npoints_cls1

        else:

            min_value = self.df_selected[feature].min()
            max_value = self.df_selected[feature].max()
            shade_cls0 = 0.7*(self.df_selected.loc[self.df_selected['Structure'] == 'RS'][feature].to_numpy() - min_value)/\
                         (max_value-min_value)
            shade_cls1 = 0.7*(self.df_selected.loc[self.df_selected['Structure'] == 'ZB'][feature].to_numpy() - min_value)/\
                         (max_value-min_value)

            if gradient == 'Grey scale':
                for i, e in enumerate(shade_cls0):
                    value = 255*(0.7-e)
                    string = 'rgb('+str(value)+","+str(value)+","+str(value)+')'
                    self.colors_cls0[i] = string
                for i, e in enumerate(shade_cls1):
                    value = 255*(0.7-e)
                    string = 'rgb('+str(value)+","+str(value)+","+str(value)+')'
                    self.colors_cls1[i] = string

            if gradient == 'Purple scale':
                for i, e in enumerate(shade_cls0):
                    value = 255 * (0.7 - e)
                    string = 'rgb(' + str(value) + "," + str(0) + "," + str(value) + ')'
                    self.colors_cls0[i] = string
                for i, e in enumerate(shade_cls1):
                    value = 255 * (0.7 - e)
                    string = 'rgb(' + str(value) + "," + str(0) + "," + str(value) + ')'
                    self.colors_cls1[i] = string

            if gradient == 'Turquoise scale':
                for i, e in enumerate(shade_cls0):
                    value = 255 * (0.7 - e)
                    string = 'rgb(' + str(0) + "," + str(value) + "," + str(value) + ')'
                    self.colors_cls0[i] = string
                for i, e in enumerate(shade_cls1):
                    value = 255 * (0.7 - e)
                    string = 'rgb(' + str(0) + "," + str(value) + "," + str(value) + ')'
                    self.colors_cls1[i] = string

            shade_cls0 = 0.7 * (
                        self.df_selected.loc[self.df_selected['Structure'] == 'RS'][feature].to_numpy() - min_value) / \
                       (max_value - min_value)
            shade_cls1 = 0.7 * (
                        self.df_selected.loc[self.df_selected['Structure'] == 'ZB'][feature].to_numpy() - min_value) / \
                       (max_value - min_value)
            if gradient == 'Blue to green':
                for i, e in enumerate(shade_cls0):
                    value = 255 * e
                    value2 = 255 - value
                    string = 'rgb(' + str(0) + "," + str(value) + "," + str(value2) + ')'
                    self.colors_cls0[i] = string
                for i, e in enumerate(shade_cls1):
                    value = 255 * e
                    value2 = 255 - value
                    string = 'rgb(' + str(0) + "," + str(value) + "," + str(value2) + ')'
                    self.colors_cls1[i] = string

            if gradient == 'Blue to red':
                for i, e in enumerate(shade_cls0):
                    value = 255 * e
                    value2 = 255 - value
                    string = 'rgb(' + str(value) + "," + str(0) + "," + str(value2) + ')'
                    self.colors_cls0[i] = string
                for i, e in enumerate(shade_cls1):
                    value = 255 * e
                    value2 = 255 - value
                    string = 'rgb(' + str(value) + "," + str(0) + "," + str(value2) + ')'
                    self.colors_cls1[i] = string

            if gradient == 'Green to red':
                for i, e in enumerate(shade_cls0):
                    value = 255 * e
                    value2 = 255 - value
                    string = 'rgb(' + str(value) + "," + str(value2) + "," + str(0) + ')'
                    self.colors_cls0[i] = string
                for i, e in enumerate(shade_cls1):
                    value = 255 * e
                    value2 = 255 - value
                    string = 'rgb(' + str(value) + "," + str(value2) + "," + str(0) + ')'
                    self.colors_cls1[i] = string

    def handle_xfeat_change(self, change):
        # changes the feature plotted on the x-axis
        # separating line is modified accordingly
        self.fig.update_layout(
            xaxis_title=change.new,
        )
        self.scatter_cls0['x'] = self.df_selected.loc[self.df_selected['Structure'] == 'RS'][change.new].to_numpy()
        self.scatter_cls1['x'] = self.df_selected.loc[self.df_selected['Structure'] == 'ZB'][change.new].to_numpy()
        line_x, line_y = self.f_x(change.new, self.widg_featy.value)
        self.scatter_hull0['x'] = line_x
        self.scatter_hull0['y'] = line_y
        min_x = min(min(self.scatter_cls0['x']), min(self.scatter_cls1['x']))
        max_x = max(max(self.scatter_cls0['x']), max(self.scatter_cls1['x']))
        min_delta = 0.05 * abs(max_x - min_x)
        self.fig.layout['xaxis'].range = [min_x - min_delta, max_x + min_delta]

    def handle_yfeat_change(self, change):
        # changes the feature plotted on the x-axis
        # separating line is modified accordingly
        self.fig.update_layout(
            yaxis_title=change.new,
        )
        self.scatter_cls0['y'] = self.df_selected.loc[self.df_selected['Structure'] == 'RS'][change.new].to_numpy()
        self.scatter_cls1['y'] = self.df_selected.loc[self.df_selected['Structure'] == 'ZB'][change.new].to_numpy()

        line_x, line_y = self.f_x(self.widg_featx.value, change.new)

        self.scatter_hull0['x'] = line_x
        self.scatter_hull0['y'] = line_y
        min_y = min(min(self.scatter_cls0['y']), min(self.scatter_cls1['y']))
        max_y = max(max(self.scatter_cls0['y']), max(self.scatter_cls1['y']))
        min_delta = 0.05 * abs(max_y - min_y)
        self.fig.layout['yaxis'].range = [min_y - min_delta, max_y + min_delta]

    def handle_markerfeat_change(self, change):
        self.set_markers_size(feature=change.new)
        self.update_markers()

    def handle_colorfeat_change(self, change):
        if change.new == 'Default color':
            self.widg_gradient.layout.visibility = 'hidden'
            self.colors_cls0 = [self.color_cls0] * self.npoints_cls0
            self.colors_cls1 = [self.color_cls1] * self.npoints_cls1
        else:
            self.widg_gradient.layout.visibility = 'visible'
            self.make_colors(feature=change.new, gradient=self.widg_gradient.value)
        self.update_markers()

    def handle_gradient_change(self, change):
        self.make_colors(feature=self.widg_featcolor.value, gradient=change.new)
        self.update_markers()

    def display_button_l_clicked(self, button):

        # Actions are performed only if the string inserted in the text widget corresponds to an existing compound
        if self.widg_compound_text_l.value in self.df_selected['Chem Formula'].tolist():
            structure_l = self.df_selected[self.df_selected['Chem Formula'] ==
                                           self.widg_compound_text_l.value]['Structure'].values[0]
            self.viewer_l.script(
                "load data/insulators_discovery/structures/" + structure_l + "_structures/"
                + self.widg_compound_text_l.value + ".xyz")

            symbols_cls0 = self.symbols_cls0
            symbols_cls1 = self.symbols_cls1

            try:
                point = symbols_cls0.index('x')
                symbols_cls0[point] = self.marker_symbol_cls0
            except:
                try:
                    point = symbols_cls1.index('x')
                    symbols_cls1[point] = self.marker_symbol_cls1
                except:
                    pass
            if structure_l == 'RS':
                point = np.where(self.scatter_cls0['text'] == self.widg_compound_text_l.value)[0][0]
                symbols_cls0[point] = 'x'
            if structure_l == 'ZB':
                point = np.where(self.scatter_cls1['text'] == self.widg_compound_text_l.value)[0][0]
                symbols_cls1[point] = 'x'

            self.symbols_cls1 = symbols_cls1
            self.symbols_cls0 = symbols_cls0
            self.set_markers_size(feature=self.widg_featmarker.value)
            self.update_markers()

    def display_button_r_clicked(self, button):

        # Actions are performed only if the string inserted in the text widget corresponds to an existing compound
        if self.widg_compound_text_r.value in self.df_selected['Chem Formula'].tolist():
            structure_r = self.df_selected[self.df_selected['Chem Formula'] ==
                                           self.widg_compound_text_r.value]['Structure'].values[0]
            self.viewer_r.script(
                "load data/insulators_discovery/structures/" + structure_r + "_structures/"
                + self.widg_compound_text_r.value + ".xyz")

            symbols_cls0 = self.symbols_cls0
            symbols_cls1 = self.symbols_cls1

            try:
                point = symbols_cls0.index('cross')
                symbols_cls0[point] = self.marker_symbol_cls0
            except:
                try:
                    point = symbols_cls1.index('cross')
                    symbols_cls1[point] = self.marker_symbol_cls1
                except:
                    pass
            if structure_r == 'RS':
                point = np.where(self.scatter_cls0['text'] == self.widg_compound_text_r.value)[0][0]
                symbols_cls0[point] = 'cross'
            if structure_r == 'ZB':
                point = np.where(self.scatter_cls1['text'] == self.widg_compound_text_r.value)[0][0]
                symbols_cls1[point] = 'cross'

            self.symbols_cls0 = symbols_cls0
            self.symbols_cls1 = symbols_cls1
            self.set_markers_size(feature=self.widg_featmarker.value)
            self.update_markers()

    def updatecolor_button_clicked(self, button):

        if self.widg_featcolor.value == 'Default color':
            try:
                self.scatter_cls0.update(marker=dict(color=self.widg_color_cls0.value))
                self.color_cls0 = self.widg_color_cls0.value
                self.colors_cls0 = self.npoints_cls0 * [self.color_cls0]
            except:
                pass
            try:
                self.scatter_cls1.update(marker=dict(color=self.widg_color_cls1.value))
                self.color_cls0 = self.widg_color_cls0.value
                self.colors_cls0 = self.npoints_cls0 * [self.color_cls0]
            except:
                pass

            if self.bg_toggle:
                try:
                    self.fig.update_layout(plot_bgcolor=self.widg_bgcolor.value)
                    self.bg_color = self.widg_bgcolor.value
                except:
                    pass

    def handle_fontfamily_change(self, change):

        self.fig.update_layout(
            font=dict(family=change.new)
        )

    def handle_fontsize_change(self, change):

        self.fig.update_layout(
            font=dict(size=change.new)
        )

    def handle_markersize_change(self, change):

        self.marker_size = int(change.new)
        self.set_markers_size(feature=self.widg_featmarker.value)
        self.update_markers()

    def handle_crossize_change(self, change):

        self.cross_size = int(change.new)
        self.set_markers_size(feature=self.widg_featmarker.value)
        self.update_markers()

    def handle_linewidth_change(self, change):

        self.line_width = change.new
        with self.fig.batch_update():
            self.scatter_hull0.line.width = change.new
            self.scatter_hull1.line.width = change.new

    def handle_linestyle_change(self, change):

        with self.fig.batch_update():
            self.scatter_hull0.line.dash = change.new
            self.scatter_hull1.line.dash = change.new

    def handle_markersymbol_cls0_change(self, change):

        for i, e in enumerate(self.symbols_cls0):
            if e == self.marker_symbol_cls0:
                self.symbols_cls0[i] = change.new
        self.marker_symbol_cls0 = change.new
        self.update_markers()

    def handle_markersymbol_cls1_change(self, change):

        for i, e in enumerate(self.symbols_cls1):
            if e == self.marker_symbol_cls1:
                self.symbols_cls1[i] = change.new
        self.marker_symbol_cls1 = change.new
        self.update_markers()

    def bgtoggle_button_clicked(self, button):

        if self.bg_toggle:
            self.bg_toggle = False
            self.fig.update_layout(
                plot_bgcolor='white',
                xaxis=dict(gridcolor='rgb(229,236,246)', showgrid=True, zeroline=False),
                yaxis=dict(gridcolor='rgb(229,236,246)', showgrid=True, zeroline=False),
            )
        else:
            self.bg_toggle = True
            self.fig.update_layout(
                plot_bgcolor=self.widg_bgcolor.value,
                xaxis=dict(gridcolor='white'),
                yaxis=dict(gridcolor='white')
            )

    def print_button_clicked(self, button):

        self.widg_print_out.clear_output()
        text = "A download link will appear soon."
        with self.widg_print_out:
            print(text)
        path = "./data/insulators_discovery/plots/"
        try:
            os.mkdir(path)
        except:
            pass
        file_name = self.widg_plot_name.value + '.' + self.widg_plot_format.value
        self.fig.write_image(path + file_name, scale=self.widg_scale.value)
        self.widg_print_out.clear_output()
        with self.widg_print_out:
            local_file = FileLink(path + file_name, result_html_prefix="Click here to download: ")
            display(local_file)

    def reset_button_clicked(self, button):

        self.symbols_cls0 = [self.marker_symbol_cls0] * self.npoints_cls0
        self.symbols_cls1 = [self.marker_symbol_cls1] * self.npoints_cls1
        self.set_markers_size(self.widg_featmarker.value)
        self.update_markers()

    def plotappearance_button_clicked(self, button):
        if self.widg_box_utils.layout.visibility == 'visible':
            self.widg_box_utils.layout.visibility = 'hidden'
            for i in range(420, -1, -1):
                self.widg_box_viewers.layout.top = str(i) + 'px'

            self.widg_box_utils.layout.bottom = '0px'
        else:
            for i in range(421):
                self.widg_box_viewers.layout.top = str(i) + 'px'
            self.widg_box_utils.layout.bottom = '400px'
            self.widg_box_utils.layout.visibility = 'visible'

    def handle_checkbox_l(self, change):
        if change.new:
            self.widg_checkbox_r.value = False
        else:
            self.widg_checkbox_r.value = True

    def handle_checkbox_r(self, change):
        if change.new:
            self.widg_checkbox_l.value = False
        else:
            self.widg_checkbox_l.value = True

    def view_structure_cls0_l(self, formula):
        self.viewer_l.script("load data/insulators_discovery/structures/RS_structures/" + formula + ".xyz")

    def view_structure_cls0_r(self, formula):
        self.viewer_r.script("load data/insulators_discovery/structures/RS_structures/" + formula + ".xyz")

    def view_structure_cls1_l(self, formula):
        self.viewer_l.script("load data/insulators_discovery/structures/ZB_structures/" + formula + ".xyz")

    def view_structure_cls1_r(self, formula):
        self.viewer_r.script("load data/insulators_discovery/structures/ZB_structures/" + formula + ".xyz")

    def update_point_cls0(self, trace, points, selector):
        # changes the points labeled with a cross on the map.
        if not points.point_inds:
            return

        symbols_cls0 = self.symbols_cls0
        symbols_cls1 = self.symbols_cls1

        # The element previously marked with x/cross is marked with circle as default value
        if self.widg_checkbox_l.value:
            try:
                point = symbols_cls0.index('x')
                symbols_cls0[point] = self.marker_symbol_cls0
            except:
                try:
                    point = symbols_cls1.index('x')
                    symbols_cls1[point] = self.marker_symbol_cls1
                except:
                    pass
        if self.widg_checkbox_r.value:
            try:
                point = symbols_cls0.index('cross')
                symbols_cls0[point] = self.marker_symbol_cls0
            except:
                try:
                    point = symbols_cls1.index('cross')
                    symbols_cls1[point] = self.marker_symbol_cls1
                except:
                    pass

        if self.widg_checkbox_l.value:
            symbols_cls0[points.point_inds[0]] = 'x'
        if self.widg_checkbox_r.value:
            symbols_cls0[points.point_inds[0]] = 'cross'

        self.symbols_cls0 = symbols_cls0
        self.symbols_cls1 = symbols_cls1
        self.set_markers_size(feature=self.widg_featmarker.value)
        self.update_markers()

        formula = trace['text'][points.point_inds[0]]
        if self.widg_checkbox_l.value:
            self.widg_compound_text_l.value = formula
            self.view_structure_cls0_l(formula)
        if self.widg_checkbox_r.value:
            self.widg_compound_text_r.value = formula
            self.view_structure_cls0_r(formula)

    def update_point_cls1(self, trace, points, selector):
        if not points.point_inds:
            return

        symbols_cls0 = self.symbols_cls0
        symbols_cls1 = self.symbols_cls1

        # The element previously marked with x/cross is marked with circle as default value
        if self.widg_checkbox_l.value:
            try:
                point = symbols_cls0.index('x')
                symbols_cls0[point] = self.marker_symbol_cls0
            except:
                try:
                    point = symbols_cls1.index('x')
                    symbols_cls1[point] = self.marker_symbol_cls1
                except:
                    pass
        if self.widg_checkbox_r.value:
            try:
                point = symbols_cls0.index('cross')
                symbols_cls0[point] = self.marker_symbol_cls0
            except:
                try:
                    point = symbols_cls1.index('cross')
                    symbols_cls1[point] = self.marker_symbol_cls1
                except:
                    pass

        if self.widg_checkbox_l.value:
            symbols_cls1[points.point_inds[0]] = 'x'
        if self.widg_checkbox_r.value:
            symbols_cls1[points.point_inds[0]] = 'cross'

        self.symbols_cls0 = symbols_cls0
        self.symbols_cls1 = symbols_cls1
        self.set_markers_size(feature=self.widg_featmarker.value)
        self.update_markers()

        formula = trace['text'][points.point_inds[0]]
        if self.widg_checkbox_l.value:
            self.widg_compound_text_l.value = formula
            self.view_structure_cls1_l(formula)
        if self.widg_checkbox_r.value:
            self.widg_compound_text_r.value = formula
            self.view_structure_cls1_r(formula)

    def show(self):

        self.widg_featx.observe(self.handle_xfeat_change, names='value')
        self.widg_featy.observe(self.handle_yfeat_change, names='value')
        self.widg_featmarker.observe(self.handle_markerfeat_change, names='value')
        self.widg_featcolor.observe(self.handle_colorfeat_change, names='value')
        self.widg_gradient.observe(self.handle_gradient_change, names='value')
        self.widg_checkbox_l.observe(self.handle_checkbox_l, names='value')
        self.widg_checkbox_r.observe(self.handle_checkbox_r, names='value')
        self.widg_display_button_l.on_click(self.display_button_l_clicked)
        self.widg_display_button_r.on_click(self.display_button_r_clicked)
        self.widg_updatecolor_button.on_click(self.updatecolor_button_clicked)
        self.widg_reset_button.on_click(self.reset_button_clicked)
        self.widg_print_button.on_click(self.print_button_clicked)
        self.widg_bgtoggle_button.on_click(self.bgtoggle_button_clicked)
        self.widg_linestyle.observe(self.handle_linestyle_change, names='value')
        self.scatter_cls0.on_click(self.update_point_cls0)
        self.scatter_cls1.on_click(self.update_point_cls1)
        self.widg_markersize.observe(self.handle_markersize_change, names='value')
        self.widg_crosssize.observe(self.handle_crossize_change, names='value')
        self.widg_linewidth.observe(self.handle_linewidth_change, names='value')
        self.widg_fontfamily.observe(self.handle_fontfamily_change, names='value')
        self.widg_fontsize.observe(self.handle_fontsize_change, names='value')
        self.widg_plotutils_button.on_click(self.plotappearance_button_clicked)
        self.widg_markersymbol_cls1.observe(self.handle_markersymbol_cls1_change, names='value')
        self.widg_markersymbol_cls0.observe(self.handle_markersymbol_cls0_change, names='value')

        self.output_l.layout = widgets.Layout(width="400px", height='350px')
        self.output_r.layout = widgets.Layout(width="400px", height='350px')

        with self.output_l:
            display(self.viewer_l)
        with self.output_r:
            display(self.viewer_r)

        self.widg_box_utils.layout.visibility = 'hidden'
        self.widg_gradient.layout.visibility = 'hidden'

        self.box_feat.layout.height = '110px'
        self.box_feat.layout.top = '30px'
        self.widg_plotutils_button.layout.left = '50px'

        self.widg_box_utils.layout.border = 'dashed 1px'
        self.widg_box_utils.right = '100px'
        self.widg_box_utils.layout.max_width = '700px'

        container = widgets.VBox([
                                  self.box_feat, self.fig,
                                  self.widg_plotutils_button,
                                  self.widg_box_viewers,
                                  self.widg_box_utils
                                  ])

        display(container)

    def instantiate_widgets(self):

        self.widg_featx = widgets.Dropdown(
            description='x-axis',
            options=self.features,
            value=self.features[0]
        )
        self.widg_featy = widgets.Dropdown(
            description='y-axis',
            options=self.features,
            value=self.features[1]
        )
        self.widg_featmarker = widgets.Dropdown(
            description="Marker",
            options=['Default size'] + self.features,
            value='Default size',
        )
        self.widg_featcolor = widgets.Dropdown(
            description='Color',
            options=['Default color'] + self.features,
            value='Default color'
        )
        self.widg_gradient = widgets.Dropdown(
            description='-gradient',
            options=self.gradient_list,
            value='Grey scale',
            layout=widgets.Layout(width='150px', right='20px')
        )
        self.widg_compound_text_l = widgets.Combobox(
            placeholder='...',
            description='Compound:',
            options=self.compounds_list,
            disabled=False,
            layout=widgets.Layout(width='200px')
        )
        self.widg_compound_text_r = widgets.Combobox(
            placeholder='...',
            description='Compound:',
            options=self.compounds_list,
            disabled=False,
            layout=widgets.Layout(width='200px')
        )
        self.widg_display_button_l = widgets.Button(
            description="Display",
            layout=widgets.Layout(width='100px')
        )
        self.widg_display_button_r = widgets.Button(
            description="Display",
            layout=widgets.Layout(width='100px')
        )
        self.widg_checkbox_l = widgets.Checkbox(
            value=True,
            indent=False,
            layout=widgets.Layout(width='50px')
        )
        self.widg_checkbox_r = widgets.Checkbox(
            value=False,
            indent=False,
            layout=widgets.Layout(width='50px'),
        )
        self.widg_markersize = widgets.BoundedIntText(
            placeholder=str(self.marker_size),
            description='Marker size',
            value=str(self.marker_size),
            layout=widgets.Layout(left='30px', width='200px')
        )
        self.widg_crosssize = widgets.BoundedIntText(
            placeholder=str(self.cross_size),
            description='Cross size',
            value=str(self.cross_size),
            layout=widgets.Layout(left='30px', width='200px')
        )
        self.widg_fontsize = widgets.BoundedIntText(
            placeholder=str(self.font_size),
            description='Font size',
            value=str(self.font_size),
            layout = widgets.Layout(left='30px', width='200px')
        )
        self.widg_linewidth = widgets.BoundedIntText(
            placeholder=str(self.line_width),
            description='Line width',
            value=str(self.line_width),
            layout = widgets.Layout(left='30px', width='200px')
        )
        self.widg_linestyle = widgets.Dropdown(
            options=self.line_styles,
            description='Line style',
            value=self.line_styles[0],
            layout=widgets.Layout(left='30px', width='200px')
        )
        self.widg_fontfamily = widgets.Dropdown(
            options=self.font_families,
            description='Font family',
            value=self.font_families[0],
            layout=widgets.Layout(left='30px', width='200px')
        )

        self.widg_bgcolor = widgets.Text(
            placeholder=str(self.bg_color),
            description='Background',
            value=str(self.bg_color),
            layout=widgets.Layout(left='30px', width='200px'),

        )
        self.widg_color_cls0 = widgets.Text(
            placeholder=str(self.color_cls0),
            description='RS color',
            value=str(self.color_cls0),
            layout=widgets.Layout(left='30px', width='200px'),
        )
        self.widg_color_cls1 = widgets.Text(
            placeholder=str(self.color_cls1),
            description='ZB color',
            value=str(self.color_cls1),
            layout=widgets.Layout(left='30px', width='200px'),
        )
        self.widg_markersymbol_cls0 = widgets.Dropdown(
            description='RS symbol',
            options=self.symbols,
            value=self.marker_symbol_cls0,
            layout=widgets.Layout(left='30px', width='200px')
        )
        self.widg_markersymbol_cls1 = widgets.Dropdown(
            description='ZB symbol',
            options=self.symbols,
            value=self.marker_symbol_cls1,
            layout=widgets.Layout(left='30px', width='200px')
        )
        self.widg_bgtoggle_button = widgets.Button(
            description='Toggle on/off background',
            layout=widgets.Layout(left='50px', width='200px'),
        )
        self.widg_updatecolor_button = widgets.Button(
            description='Update colors',
            layout=widgets.Layout(left='50px', width='200px')
        )
        self.widg_reset_button = widgets.Button(
            description='Reset symbols',
            layout=widgets.Layout(left='50px',width='200px')
        )
        self.widg_plot_name = widgets.Text(
            placeholder='plot',
            value='plot',
            description='Name',
            layout=widgets.Layout(width='300px')
        )
        self.widg_plot_format = widgets.Text(
            placeholder='png',
            value='png',
            description='Format',
            layout=widgets.Layout(width='150px')
        )
        self.widg_scale = widgets.Text(
            placeholder='1',
            value='1',
            description="Scale",
            layout=widgets.Layout(width='150px')
        )
        self.widg_print_button = widgets.Button(
            description='Print',
            layout=widgets.Layout(left='50px', width='600px')
        )
        self.widg_print_out = widgets.Output(
            layout=widgets.Layout(left='150px', width='400px')
        )
        self.widg_printdescription = widgets.Label(
            value="Click 'Print' to export the plot in the desired format.",
            layout=widgets.Layout(left='50px', width='640px')

        )
        self.widg_printdescription2 = widgets.Label(
            value="The resolution of the image can be increased by increasing the 'Scale' value.",
            layout = widgets.Layout(left='50px', width='640px')
        )
        self.widg_featuredescription = widgets.Label(
            value="The dropdown menus select the features to visualize."
        )
        self.widg_description = widgets.Label(
            value='Tick the box next to the cross symbols in order to choose which windows visualizes the next '
                  'structure selected in the map above.'
        )
        self.widg_colordescription = widgets.Label(
            value='Colors in the boxes below can be written as a text string, i.e. red, '
                  'green,...,  or in a rgb/a, hex format. ',
            layout=widgets.Layout(left='50px', width='640px')

        )
        self.widg_colordescription2 = widgets.Label(
            value="After modifying a specific field, click on the 'Update colors' button to display the changes in "
                  "the plot.",
            layout=widgets.Layout(left='50px', width='640px')
        )
        self.widg_plotutils_button = widgets.Button(
            description='Toggle on/off the plot appearance utils',
            layout=widgets.Layout(width='600px')
        )
        self.widg_box_utils = widgets.VBox([widgets.HBox([self.widg_markersize, self.widg_crosssize,
                                                          self.widg_markersymbol_cls0]),
                                            widgets.HBox([self.widg_linewidth, self.widg_linestyle,
                                                          self.widg_markersymbol_cls1]),
                                            widgets.HBox([self.widg_fontsize, self.widg_fontfamily]),
                                            self.widg_colordescription, self.widg_colordescription2,
                                            widgets.HBox([self.widg_color_cls0, self.widg_color_cls1, self.widg_bgcolor]),
                                            widgets.HBox([self.widg_bgtoggle_button,self.widg_updatecolor_button,
                                                          self.widg_reset_button]),
                                            self.widg_printdescription, self.widg_printdescription2,
                                            widgets.HBox([self.widg_plot_name, self.widg_plot_format, self.widg_scale]),
                                            self.widg_print_button, self.widg_print_out,
                                            ])

        file1 = open("./assets/insulators_discovery/cross.png", "rb")
        image1 = file1.read()
        self.widg_img1 = widgets.Image(
            value=image1,
            format='png',
            width=30,
            height=30,
        )
        file2 = open("./assets/insulators_discovery/cross2.png", "rb")
        image2 = file2.read()
        self.widg_img2 = widgets.Image(
            value=image2,
            format='png',
            width=30,
            height=30,
        )
        self.output_l = widgets.Output()
        self.output_r = widgets.Output()

        self.box_feat = widgets.HBox([widgets.VBox([self.widg_featx, self.widg_featy]),
                                 widgets.VBox([self.widg_featmarker,
                                               widgets.HBox([self.widg_featcolor, self.widg_gradient])
                                               ])
                                 ])

        self.widg_box_viewers = widgets.VBox([self.widg_description, widgets.HBox([
            widgets.VBox([
                widgets.HBox([self.widg_compound_text_l, self.widg_display_button_l,
                              self.widg_img1, self.widg_checkbox_l]),
                self.output_l]),
            widgets.VBox(
                [widgets.HBox([self.widg_compound_text_r, self.widg_display_button_r,
                               self.widg_img2, self.widg_checkbox_r]),
                 self.output_r])
        ])])
